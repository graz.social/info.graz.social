---
title: "Impressum"
ShowBreadCrumbs: false
---

### Postanschrift

```
graz.social - Verein zur Förderung ethischer Digitalkultur
Rechbauerstraße 19a/3
8010 Graz
ÖSTERREICH
```

### E-Mail
<a class="bot">laicos.zarg@riw</a>

### Vereinsdaten

**Zuständigkeit:** Landespolizeidirektion Steiermark, SVA 3  
**ZVR-Zahl:** 1309075088
