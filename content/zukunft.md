---
title: "Zukunft von graz.social"
ShowBreadCrumbs: true
date: 2023-01-21
author: André Menrath
---

## Update am 10.3.2023
Es wurde ein vorübergehende Lösung bis Ende Sommer 2023 gefunden.
https://graz.social/@wir/109938046327853221

Diese Seite bzw. dieser Beitrag enstand zwei Tage nach einem Koordinationstreffen am Abend des 18. Jänner 2023 zusammen mit Menschen von [mur.at](https://mur.at) (3), [gemeinsam.jetzt](https://gemeinsam.jetzt) (1), der [Kometin](https://kometin.at) (1) und [graz.social](http://localhost:1313/ueber-uns/#personen) (4) in den wunderschönen Räumen der Kometin. An dieser Stelle herzlichen Dank an alle Teilnehmenden für ihre beratenden Stimmen, inbesondere im Hinblick auf die Zukunft von graz.social, und der Kometin für den Raum, der ein Treffen in toller Atmosphäre möglich gemacht hat.

## Hintergrund
Unverändert wird André, wie [auf Mastodon im November 2022 bekannt gegeben](https://graz.social/@wir/109372229117971918) graz.social ab spätestens Mitte/Ende Mai 2023 nicht mehr weiter leiten. Im besten Fall würde eine Übergabe der Letztverantwortung mit einer großzugigen Frist (drei Monate) einhergehen, um den Nutzer\*innen möglichst gut die Wahl zu lassen ihre Konten zu schließen und oder umzuziehen. Eine Übergabe an eine Privatperson würde größtes Vertrauen voraussetzen und steht auch nicht in Aussicht, bis dato hätte sich auch noch keine Person gemeldet. Eine Übergabe an einen neu zu gründenden Verein wäre die bei weitem bevorzugte Lösung.

## Status der Vereinsgründung
Das Gründungsdokument wurde am 12. September 2023 in korrigierter Fassung eingereicht.

## Wieso ein Verein
Die Gründung eines Vereins erscheint unabdingbar, um den Dienst graz.social langfristig betreiben zu können. Ein Verein würde es ermöglichen noch demokratischer zu agieren, größere Spenden verantwortungsvoll zu verwalten und den Moderator\*innen und Administrator*innen gegebenenfalls transparent kleinere Aufwandsentschädigungen zu zahlen. Darüber hinaus hat es sich als Wunsch herausgestellt, im Rahmen eines Vereins in Zukunft auch eine kleine Stelle zu finanzieren, welche für Bildungs- und Öffentlichkeitsarbeit im Tätigkeitsbereich von graz.social zuständig ist.

### Personalsitutation
An Menschen, welche bereit sind in der Moderation und Administration (technische Infrastruktur) mitzuhelfen und im Verein als ordentliche Mitglieder\*innen zu wirken gibt es derzeit keinen Mangel. Wir würden uns aber wünschen mehr Frauen und diverse Personen mit im Team zu haben.

### Mögliche Organisationspläne
Am sinnvollsten erscheint es uns, wenn alle Moderator\*innen und Admins ordentliche Vereinsmitglieder wären. Speziell auf Mastodon wäre es möglich, auch Rollen wie Co-Moderator*innen oder Moderationsprüferinnen einzurichten, welche es de facto schon gibt, aber es noch keinen Bedarf gab, die konkreten Abläufe explizit schriftlich zu regeln.

### Entwurf der Statuten
Einen Entwurf der Statuten gibt es bereits, ihr findet ihn [hier](/statuten).
