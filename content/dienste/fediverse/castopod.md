---
title: Castopod
subtitle: "Podcast und Social Media in einem"
type: fediverse
# description: "Alternative zu Soundcloud, Spotify, Apple Podcasts"
featured-image: castopod.webp
link: "https://podcast.graz.social"
weight: 3
tags: ["Fediverse", "Soziales-Netzwerk", "Podcast"]
replaces: ["Soundcloud", "Spotify", "Apple Podcasts"]
label:
    replacesTitle: true
    name: castopod
    height: 37
---

Castopod ist ein Gemeinschaftsprojekt, das es dir ermöglicht, Podcasts zu veröffentlichen. Dein Podcast kann dabei bequem über alle verfügbaren Apps abboniert werden und ist zugleich dein Social Media. Andere Fediverse Nutzer*innen können mit dir reibungslos interagieren.

## Screenshot
![Castopod](../images/screenshots/castopod.webp)

## Registrierung
Wenn du deinen Podcast gerne mit Hilfe von Castopod unter einer Identität von @castopod.graz.social veröffentlichen würdest bekommst du auf Anfrage per Mail oder via Direktnachricht [an unseren Mastodon-Account](https://graz.social/@wir) einen Account von uns eingerichtet.

## Links
https://castopod.org