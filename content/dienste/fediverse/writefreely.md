---
title: WriteFreely
link: "https://write.graz.social"
type: fediverse
weight: 0
tags: ["Fediverse", "Blog", "Literatur", "Journalismus"]
replaces: ["WordPress", "Medium"]
label:
    name: writefreely
    replacesTitle: true
    height: 33
---

> Für das Schreiben von Texten aller Art, z.B deinen eigenen Blog

WriteFreely ist komplett auf das Verfassen von Texten ausgerichtet. Es gibt keinen News-Feed, keine Benachrichtigungen oder unnötige Likes oder Kommentare, die dich von deinem Gedankengang ablenkt. Du erhältst eine ablenkungsfreie Schreibumgebung, und die Besucher können ein sauberes Leseerlebnis genießen.

## Erreiche ein riesiges dezentrales Netzwerk

Erreiche auch außerhalb deiner eigenen Website mit der Föderation über ActivityPub. WriteFreely kann mit anderen föderierten Plattformen wie Mastodon kommunizieren, so dass Menschen deinen Blogs folgen, ihre Lieblingsposts als Favoriten markieren und sie mit ihren Follower teilen können.

## Mehr Informationen auf der Seite der Entwickler:
https://writefreely.org/

## Registrierung
Da WriteFreely noch über keine E-Mail-Verifizierung verfügt und die Moderationstool noch sehr einfach gehalten sind ist die selbstständige Registrierung momentan geschlossen. Wenn du gerne einen Account hättest, kontaktiere uns einfach persönlich.