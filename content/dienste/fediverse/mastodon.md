---
title: "Mastodon"
type: fediverse
link: "https://graz.social"
weight: 1
replaces: ["Twitter","Facebook"]
tags: ["Fediverse", "Soziales-Netzwerk","Micro-Blogging"]
label:
    replacesTitle: true
    name: "mastodon"
    height: 39
---

> Der Allrounder: kurze Texte, Bilder und Videos, sowie die wohl beste Plattform für passive Leser\*innen!

Ein soziales Netzwerk in unseren Händen. Folge Freunden und entdecke neue, mit mehr als 5,2 Millionen Menschen. Veröffentliche alles was du willst: Links, Fotos, Text, Videos. Alles auf einer Plattform, die gemeinschaftsbasiert und werbefrei ist.

## Beschreibung

> Förderation - dezentral, aber Vernetzt

Genau wie beim Registrieren einer E-Mail-Adresse wird graz.social deinen Account hosten und Teil deiner Identität sein. **Und genauso kannst du allen folgen und mit jedem schreiben, egal von welchen Server aus!**

{{< peertube "videos.im.allmendenetz.de" "09718999-9718-4d01-b9a7-979be3e717a5" >}}

Neben Graz social stehen dir viele andere Zuhause für deinen Mastodon Account offen. Hier gibt es eine offizielle Liste: https://joinmastodon.org/communities

Mastodon ist, wie bereits gesagt, nicht nur mit anderen Mastodon Instanzen vernetzt, sondern mit allen sozialen Netzwerken des **Fediverse**. Im Bild unten sieht man, dass man auch zum Beispiel auch einem **PeerTube**-Account genauso folgen kann.

<!-- ## Screenshot
![Screenshot der Mastodon Web-Oberfläche](../images/screenshots/mastodon.jpg) -->

## Umziehen
Man kann bei Mastodon darüber hinaus auch umziehen, also seine Follower und Folgenden zu einem neuen Server mitnehmen: die Wahl des "Zuhauses" ist nicht endgültig!

## Registierung
Eventuell können selbstständig keine Accounts mit E-Mail-Adressen von Google, Yahoo oder Microsoft registriert werden. Diese Domains waren zuletzt für 99% des Spams verantwortlich. Wenn du keine andere E-Mail-Adresse besitzt, kontaktiere und einfach persönlich und wir erstellen dir deinen Account manuell.