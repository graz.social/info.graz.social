---
title: Nextcloud
type: tool
link: "https://cloud.graz.social"
weight: 10
tags: ["Tool", "Cloud", "Umfragen"]
replaces: ["Google-Drive", "Dropbox"]
label:
    name: nextcloud
    replacesTitle: true
    height: 80
---

Der Verein nutzt um sich selbst zu verwalten eine eigene Nextcloud Instanz. Auf Anfrage wurden in der Vergangenheit Accounts an einzelne andere Organisationen vergeben. 

## Features
- Gemeinsames Editieren von Dokumenten
- Gemeinsame Dateiablage
- Freigabe von Dateien und Ordnern
- Gutes Gruppen- und Rechtemanagement

## Registrierung
Da wir leider momentan einen Ressourcen-Engpass in der Administration haben, haben wir uns dazu entschlossen, unsere Cloud momentan nicht weiter nach außen hin zu öffnen.